var app = angular.module('ListaTelefonica', []);
app.controller('ListaTelefonicaController', function($scope){
	$scope.nome = "Lista Telefônica";
	$scope.contatos = [
		{nome: "Rafael", telefone: "986231235", operadora: "Oi"},
		{nome: "Amor", telefone: "988428408", operadora: "Oi"},
		{nome: "Mamãe Léo", telefone: "988728403", operadora: "Oi"},
		{nome: "Papai Neto", telefone: "988854350", operadora: "Oi"}
	];
	$scope.operadoras = [
		{nome: "Oi"},
		{nome: "Tim"},
		{nome: "Vivo"},
		{nome: "Claro"}
	];

	$scope.adicionaContato = function (contato) {
		$scope.contatos.push(angular.copy(contato));
		delete $scope.contato;
	};

});